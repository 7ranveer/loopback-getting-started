'use strict';

var fs = require('fs');
var path = require('path');
var async = require('async');
var outputPath = '/home/ranveer/Desktop/loopback-getting-started/common/logfiles';
var Promise = require('bluebird');
Promise.promisifyAll(fs);

module.exports = function(Purchase) {
    //using promises for series
    Purchase.promisePara = function(id){
        if(Number.isNaN(id)){
            return 'id should be a number'
        }else{
            var response= {};
            var myPromises = [];
            return new Promise(function(resolve,reject){
                Purchase.findById(id)
                    .then(function (response) {
                        if([response].includes(undefined) || [response].includes(null) ){
                           return reject('no instance found','enter a transaction that exist');
                        }else {
                            let outputName = [outputPath + '/' +response.department + '.txt',outputPath + '/master.txt'];
                            let responseData =response.transactionid+','+response.uid+','+response.pid+','+response.datetime+','+response.valid+','+response.department+','+'\n' 
                            myPromises.push(
                                fs.appendFileSync(outputName[0],responseData,"UTF-8",{'flags': 'a+'})
                            );
                            myPromises.push(
                                fs.appendFileSync(outputName[1],responseData,"UTF-8",{'flags': 'a+'})
                            );
                            return [Promise.all(myPromises),response];
                        }
                    })
                    .then(function(resolved){
                        console.log(resolved[1]);
                        var resol = resolved[0];
                        console.log('parallel execution');
                        resolve(resol);
                    })
                    .catch(function(err){
                        console.log(err);
                        reject(err);
                    });

            })
        }
    };
    Purchase.remoteMethod('promisePara', {
        accepts: {
            arg: 'id',
            type: 'number'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/promisePara',
            verb: 'post'
        }
    });
    //series using promise bluebird
    Purchase.promiseSeries = function(ids){
        return new Promise(function(resolve,reject){
            if(ids==undefined || ids==null){
                reject('input cannot be undefined or null');
            }
            var myids = ids.trim().split(',').map(Number);
            if(myids.length<1){
                return reject("array cannot be empty");
            }
            else if(!myids.every(function(id){return Number.isNaN(id)})){
                return reject('all ids sholud be number');
            }else{
                Promise.mapSeries(myids,function(id){
                    return Purchase.promisePara(id);
                }).then(function(resolved){
                    console.log('all done');
                    resolve(resolved);
                }).catch(function(err){
                    console.log(err);
                    reject();
                }) 
            }   
        })
    }
    Purchase.remoteMethod('promiseSeries', {
        accepts: {
            arg: 'ids',
            type: 'string'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/promiseSeries',
            verb: 'post'
        }
    });
    //series using async library
    Purchase.asyncSeries = function(ids,cb){
        var myids = ids.split(',').map(Number);
        console.log(myids);
        async.timesSeries(myids.length, function(n, next) {
            Purchase.promisePara(myids[n], function(err, results) {
                console.log(myids[n]);
                next(err, results);
            });
        },
        function(err, results) {
            if(err){
                cb(err,null);
            }else{
                cb(null,results);
            }
        });
    }
    Purchase.remoteMethod('asyncSeries', {
        accepts: {
            arg: 'ids',
            type: 'string'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/asyncSeries',
            verb: 'post'
        }
    });
    //parallel using promise 
    Purchase.asyncPara = function(id,cb){
        var response= {};
        var myPromises = [];
        Purchase.findById(id)
                .then(function (response) {
                    let outputName = [outputPath + '/' +response.department + '.txt',outputPath + '/master.txt'];
                    let responseData =response.transactionid+','+response.uid+','+response.pid+','+response.datetime+','+response.valid+','+response.department+','+'\n' 
                    myPromises.push(
                        fs.appendFileSync(outputName[0],responseData,"UTF-8",{'flags': 'a+'})
                    );
                    myPromises.push(
                        fs.appendFileSync(outputName[1],responseData,"UTF-8",{'flags': 'a+'})
                    );
                    return [Promise.all(myPromises),response];
                })
                .then(function(resolve){
                    console.log(resolve[1]);
                    console.log('parallel execution');
                    return cb(null,resolve[1]);
                })
                .catch(function(reject){
                    console.log(reject);
                    return cb(null,reject[1]);
                });
    };
    Purchase.remoteMethod('asyncPara', {
        accepts: {
            arg: 'id',
            type: 'number'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/asyncPara',
            verb: 'post'
        }
    });
	Purchase.myupsert = function(myData, cb) {
        var response = myData;
        Purchase.upsert(myData, function(err, myData) {
            if (err) return console.log(err);
            response = myData;
            console.log("user updated in db");
        });
        cb(null, response);
    };
	Purchase.remoteMethod('myupsert', {
        accepts: {
            arg: 'myData',
            type: 'object'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/myupsert',
            verb: 'post'
        }
    });
	Purchase.myCreate = function(myData, cb) {
        var response = myData;
        Purchase.create([myData], function(err, myData) {
            if (err) return console.log(err);
            response = myData;
            console.log("new user created in db");
        });
        cb(null, response);
    };
	Purchase.remoteMethod('myCreate', {
        accepts: {
            arg: 'myData',
            type: 'object'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/myCreate',
            verb: 'post'
        }
    });
    Purchase.mydelete = function(id, cb){
    	var response;
        Purchase.destroyById(id, function(err, id) {
            if (err) return console.log(err);
            response = id;
            console.log("the user deleted in db");
        });
        cb(null, response);
    };
    Purchase.remoteMethod('mydelete', {
    	accepts: {
            arg: 'id',
            type: 'number'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/mydelete',
            verb: 'post'
        }
    });
	Purchase.readwithid = function(id, cb) {
    	var response;
        Purchase.findById(id, function(err, id) {
            if (err) return console.log(err);
            response = id;
            console.log(response);
        });
    	cb(null, response);
  	};
  	Purchase.remoteMethod('readwithid', {
  		accepts: {
            arg: 'id',
            type: 'number'
        },
        returns: {
        	arg: 'response',
        	type: 'object'
      	},
     	http: {
        	path: '/readwithid',
        	verb: 'post'
      	}
      	
	});
};
