'use strict';

module.exports = function(User) {
    var app = require('../../server/server');
    User.storedproc = function(id,cb) {
        var query = "CALL invaliduser(?)";
        var params = [id];
        app.datasources.mysqlDs.connector.execute(query, params, function(err, response){
            if(err){
                console.log(err);
                cb(err);
            }else{
                console.log(response);
                console.log('\n');
                cb(null,response);
            }
        });
    }
    User.remoteMethod('storedproc', {
        accepts: {
            arg: 'id',
            type: 'number'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/storedproc',
            verb: 'post'
        }
    });
    User.promiseuser = function(uid, cb) {
        var response,info;
        if([uid].includes(undefined) || [uid].includes(null) ){
            cb(null, "the input id is undefined or null value");
        }else {
            var options = {
                "validate" : true
            }
            var Purchase = app.models.Purchase;
            User.updateAll(
                {"uid": uid},
                {"valid": 0}
            )
            .then(
                function(response) {
                    console.log("step 1")
                    return Purchase.updateAll(
                            {"uid": uid},
                            {"valid": 0}
                        );
                }
            )
            .then(
                function(response){
                    console.log('step 2')
                    cb(null,response);
                }
            )
            .catch(err=>{
                console.log("we got some err "+err)
            })
        }
    };
    User.remoteMethod('promiseuser', {
        accepts: {
            arg: 'uid',
            type: 'number'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/promiseuser',
            verb: 'post'
        }
    });
    User.invaliduser = function(uid, cb) {
        var response,info;
        if([uid].includes(undefined) || [uid].includes(null) ){
            cb(null, "the input id is undefined or null value");
        }else {
            var options = {
                "validate" : true
            }
            User.updateAll(
                {"uid": uid},
                {"valid": 0},
                function(err, response) {
                    if (err) return cb(err);
                    else{
                        console.log(response); 
                        var Purchase = app.models.Purchase;
                        console.log(Purchase);
                        Purchase.updateAll(
                            {"uid": uid},
                            {"valid": 0},
                            function(err,info){
                                if(err) return cb(err);
                                else{
                                    console.log(info);
                                    cb(null, info);
                                }
                        })   
                    }
            });
            // cb(null, response);
        }
    };
    User.remoteMethod('invaliduser', {
        accepts: {
            arg: 'uid',
            type: 'number'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/invaliduser',
            verb: 'post'
        }
    });
	User.myupsert = function(uname,uid,uemail,valid, cb) {
        var response;
        if([uname,uid,uemail,valid].includes(undefined) || [uname,uid,uemail,valid].includes(null) ){
            cb(null, "the input containd undefined or null value");
        }else {
            var myData = {
                "uname": uname,
                "uid": uid,
                "uemail": uemail,
                "valid": valid
            }
            User.upsert(myData, function(err, response) {
                if (err) return console.log(err);
                console.log("user updated in db");
            });
            cb(null, response);
        }
    };
	User.remoteMethod('myupsert', {
        accepts: [{
            arg: 'uname',
            type: 'string'
        },{
            arg: 'uid',
            type: 'number'
        },{
            arg: 'uemail',
            type: 'string'
        },{
            arg: 'valid',
            type: 'number'
        }],
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/myupsert',
            verb: 'post'
        }
    });
	User.myCreate = function(uname,uid,uemail, cb) {
        var response;
        if([uname,uid,uemail].includes(undefined) || [uname,uid,uemail].includes(null) ){
            cb(null, "the input containd undefined or null value");
        }else {
            var myData = {
                "uname": uname,
                "uid": uid,
                "uemail": uemail,
                "valid": 1
            }
            User.create([myData], function(err, response) {
                if (err) return console.log(err);
                console.log("new user created in db");
            });
            cb(null, response);
        }
    };
	User.remoteMethod('myCreate', {
        accepts: [{
            arg: 'uname',
            type: 'string'
        },{
            arg: 'uid',
            type: 'number'
        },{
            arg: 'uemail',
            type: 'string'
        }],
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/myCreate',
            verb: 'post'
        }
    });
    User.mydelete = function(id, cb){
    	var response;
        User.destroyById(id, function(err, id) {
            if (err) return console.log(err);
            response = id;
            console.log("the user deleted in db");
        });
        cb(null, response);
    };
    User.remoteMethod('mydelete', {
    	accepts: {
            arg: 'id',
            type: 'number'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/mydelete',
            verb: 'post'
        }
    });
	User.readwithid = function(id, cb) {
    	var response;
        User.findById(id, function(err, id) {
            if (err) return console.log(err);
            else {
                response = id;
                console.log(response);
            }
        });
    	cb(null, response);
  	};
  	User.remoteMethod('readwithid', {
  		accepts: {
            arg: 'id',
            type: 'number'
        },
        returns: {
        	arg: 'response',
        	type: 'object'
      	},
     	http: {
        	path: '/readwithid',
        	verb: 'post'
      	}
	});
};
// module.exports = Users;