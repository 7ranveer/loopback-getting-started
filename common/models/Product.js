'use strict';

module.exports = function(Product) {
    var app = require('../../server/server');
    Product.orderbyprice = function(cb) {
        var response;
        Product.find({order: 'pid DESC'}, function(err, response) {
            if (err) return cb(err);
            else{
                console.log(response);
                cb(null,response);
            }
        });
    };
    Product.remoteMethod('orderbyprice', {
        returns: {
            arg: 'response',
            type: 'array'
        },
        http: {
            path: '/orderbyprice',
            verb: 'post'
        }
    });
	Product.myupsert = function(myData, cb) {
        var response = myData;
        Product.upsert(myData, function(err, myData) {
            if (err) return console.log(err);
            response = myData;
            console.log("product updated in db");
        });
        cb(null, response);
    };
	Product.remoteMethod('myupsert', {
        accepts: {
            arg: 'myData',
            type: 'object'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/myupsert',
            verb: 'post'
        }
    });
	Product.myCreate = function(myData, cb) {
        var response = myData;
        Product.create([myData], function(err, myData) {
            if (err) return console.log(err);
            response = myData;
            console.log("new product created in db");
        });
        cb(null, response);
    };
	Product.remoteMethod('myCreate', {
        accepts: {
            arg: 'myData',
            type: 'object'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/myCreate',
            verb: 'post'
        }
    });
    Product.mydelete = function(id, cb){
    	var response;
        Product.destroyById(id, function(err, id) {
            if (err) return console.log(err);
            response = id;
            console.log("the user deleted in db");
        });
        cb(null, response);
    };
    Product.remoteMethod('mydelete', {
    	accepts: {
            arg: 'id',
            type: 'number'
        },
        returns: {
            arg: 'response',
            type: 'string'
        },
        http: {
            path: '/mydelete',
            verb: 'post'
        }
    });
	Product.readwithid = function(id, cb) {
    	var response;
        Product.findById(id, function(err, id) {
            if (err) return console.log(err);
            response = id;
            console.log(response);
        });
    	cb(null, response);
  	};
  	Product.remoteMethod('readwithid', {
  		accepts: {
            arg: 'id',
            type: 'number'
        },
        returns: {
        	arg: 'response',
        	type: 'string'
      	},
     	http: {
        	path: '/readwithid',
        	verb: 'post'
      	}
	});
};
